import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { RegisterFormComponent } from './component/register-form/register-form.component';
import { AppRootComponent } from './component/app-root/app-root.component';
import {DropdownModule} from 'primeng/primeng';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CalendarModule} from 'primeng/calendar';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import { TutorComponent } from './component/register-form/tutor/tutor.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    RegisterFormComponent,
    AppRootComponent,
    TutorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    DropdownModule,
    CalendarModule,
    AngularFontAwesomeModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppRootComponent]
})
export class AppModule { }
