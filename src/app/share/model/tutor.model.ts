
export class TutorModel {

  private _name: string;
  private _relathion: string;
  private _phoneNumber: number | null;


  constructor() {
    this._name = '';
    this._relathion = '';
    this._phoneNumber = null;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get relathion(): string {
    return this._relathion;
  }

  set relathion(value: string) {
    this._relathion = value;
  }

  get phoneNumber(): number | null {
    return this._phoneNumber;
  }

  set phoneNumber(value: number | null) {
    this._phoneNumber = value;
  }
}
