
export class HospitalModel {

  private _name: string;

  constructor() {
    this._name = '';
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }
}
