import {SelectItem} from 'primeng/api';

export enum StatusEnum {
  grave = 'GRAVE',
  leve = 'LEVEL'
}

// tslint:disable-next-line:no-namespace
export namespace StatusEnum {

  export function getSelectedItem(): Array<SelectItem> {

    const illTypeEnumToSelectArray: Array<SelectItem> = [];

    illTypeEnumToSelectArray.push({label: StatusEnum.grave, value: StatusEnum.grave});
    illTypeEnumToSelectArray.push({label: StatusEnum.leve, value: StatusEnum.leve});

    return illTypeEnumToSelectArray;
  }

}
