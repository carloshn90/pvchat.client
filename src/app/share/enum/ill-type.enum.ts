import {SelectItem} from 'primeng/api';

export enum IllTypeEnum {

  cancer1 = 'CANCER 1',
  cancer2 = 'CANCER 2'
}

// tslint:disable-next-line:no-namespace
export namespace IllTypeEnum {

   export function getSelectedItem(): Array<SelectItem> {

    const illTypeEnumToSelectArray: Array<SelectItem> = [];

    illTypeEnumToSelectArray.push({label: IllTypeEnum.cancer1, value: IllTypeEnum.cancer1});
    illTypeEnumToSelectArray.push({label: IllTypeEnum.cancer2, value: IllTypeEnum.cancer2});

    return illTypeEnumToSelectArray;
  }

}
