import {Component, OnInit} from '@angular/core';
import {HospitalModel} from '../../share/model/hospitalModel';
import {TutorModel} from '../../share/model/tutor.model';
import {LocationModel} from '../../share/model/location.Model';
import {SelectItem} from 'primeng/api';
import {IllTypeEnum} from '../../share/enum/ill-type.enum';
import {StatusEnum} from '../../share/enum/status.enum';
import {RegisterModel} from './register.model';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  hospitalArray: Array<HospitalModel>;
  locationArray: Array<LocationModel>;
  statusArray: Array<SelectItem>;
  illTypeArray: Array<SelectItem>;

  registerModel: RegisterModel;

  constructor() {
    this.hospitalArray = [];
    this.locationArray = [];
    this.registerModel = new RegisterModel();
  }

  ngOnInit() {

    const hospital = new HospitalModel();
    hospital.name = 'Hospital las Plamas';
    this.hospitalArray.push(hospital);

    const location = new LocationModel();
    location.name = 'Las Palmas';
    this.locationArray.push(location);

    this.illTypeArray = IllTypeEnum.getSelectedItem();

    this.statusArray = StatusEnum.getSelectedItem();

    this.addTutor();
  }

  addTutor(): void {

    this.registerModel.tutorArray.push(new TutorModel());
  }

  isNecessaryShowHospital(status: StatusEnum): boolean {
    return status === StatusEnum.grave;
  }

  show(): void {
    console.log(this.registerModel);
  }

}
