import {Component, Input, OnInit} from '@angular/core';
import {TutorModel} from '../../../share/model/tutor.model';

@Component({
  selector: 'app-tutor',
  templateUrl: './tutor.component.html',
  styleUrls: ['./tutor.component.css']
})
export class TutorComponent implements OnInit {

  @Input() tutorModel: TutorModel;

  constructor() { }

  ngOnInit() {
  }

}
