import {LocationModel} from '../../share/model/location.Model';
import {StatusEnum} from '../../share/enum/status.enum';
import {HospitalModel} from '../../share/model/hospitalModel';
import {IllTypeEnum} from '../../share/enum/ill-type.enum';
import {TutorModel} from '../../share/model/tutor.model';

export class RegisterModel {

  private _childName: string;
  private _bornDate: Date | null;
  private _location: LocationModel | null;
  private _childStatus: StatusEnum | null;
  private _hospital: HospitalModel | null;
  private _illType: IllTypeEnum | null;
  private _tutorArray: Array<TutorModel>;


  constructor() {
    this._childName = '';
    this._bornDate = null;
    this._location = null;
    this._childStatus = null;
    this._hospital = null;
    this._illType = null;
    this._tutorArray = [];
  }

  get childName(): string {
    return this._childName;
  }

  set childName(value: string) {
    this._childName = value;
  }

  get bornDate(): Date | null {
    return this._bornDate;
  }

  set bornDate(value: Date | null) {
    this._bornDate = value;
  }

  get location(): LocationModel | null {
    return this._location;
  }

  set location(value: LocationModel | null) {
    this._location = value;
  }

  get childStatus(): StatusEnum | null {
    return this._childStatus;
  }

  set childStatus(value: StatusEnum | null) {
    this._childStatus = value;
  }

  get hospital(): HospitalModel | null {
    return this._hospital;
  }

  set hospital(value: HospitalModel | null) {
    this._hospital = value;
  }

  get illType(): IllTypeEnum | null {
    return this._illType;
  }

  set illType(value: IllTypeEnum | null) {
    this._illType = value;
  }

  get tutorArray(): any {
    return this._tutorArray;
  }

  set tutorArray(value: any) {
    this._tutorArray = value;
  }
}
